<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$stu_project_id=(int)$_GET['id'];
$sql="SELECT `content`, `state` FROM `stu_project` where `stu_project_id`={$stu_project_id};";
$project=$db->getOne($sql);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        $(document).ready(function () {
            UE.getEditor('contentInput');
        });
    </script>    
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="projectManager.php">项目管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <form action="projectStudentModifyAction.php" method="post">
    <fieldset>
        <legend>修改学生状态</legend>
        <input type="hidden" name="id" value="<?php echo $stu_project_id; ?>">
        <label for ="stateInput">该学生状态</label>
        <input type="text" name="state" id="stateInput" value="<?php echo $project['state'] ?>" placeholder="在做\退出">
        <label for ="contentInput">内容</label>
        <textarea name="content" id="contentInput" placeholder="请输入内容"><?php echo $project['content'] ?></textarea>
    </fieldset>
    <input type="submit">
</form>
        </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>