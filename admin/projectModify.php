<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$id=(int)$_GET['id'];
$sql="SELECT * FROM project where `project_id`={$id};";
$project=$db->getOne($sql);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        $(document).ready(function () {
            UE.getEditor('contentInput');
        });
    </script>    
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
<form action="projectModifyAction.php" method="post">
    <fieldset>
        <legend>项目基本信息</legend>
        <input type="hidden" name="id" value="<?php echo $project['project_id'] ?>">
        <label for ="titleInput">标题</label>
        <input type="text" name="title" id="titleInput" value="<?php echo $project['title'] ?>" placeholder="请输入标题">
        <label for ="start_dateInput">开始时间</label>
        <input type="text" name="start_date" id="start_dateInput" value="<?php echo $project['start_date'] ?>" placeholder="请输入开始时间">
        <label for ="end_dateInput">结束时间</label>
        <input type="text" name="end_date" id="end_dateInput" value="<?php echo $project['end_date'] ?>" placeholder="请输入结束时间">
        <label for ="expenseInput">项目经费</label>
        <input type="text" name="expense" id="expenseInput" value="<?php echo $project['expense'] ?>" placeholder="请输入项目经费">
    </fieldset>
    <fieldset>
        <legend>项目详细信息</legend>
        <label for ="contentInput">内容</label>
        <textarea name="content" id="contentInput" placeholder="请输入内容"><?php echo $project['content'] ?></textarea>
    </fieldset>
    <input type="submit">
</form>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>