<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$result=$db->getOne("select count(*) as total from `news` where 1");
if($result['total']%15==0){
    $pageNum=$result['total']/15;
}else{
    $pageNum=($result['total']/15)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*15;
$sql="SELECT `news_id`, `title`, `author`, `date` FROM `news` limit $start,15;";
$newsArray=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
            <div id="contentControl">
                <a href="newsAdd.php"><div class="button" id="contentBtnAdd"></div></a>
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                <tr>
                    <th>id</th>
                    <th>标题</th>
                    <th>作者</th>
                    <th>时间</th>
                    <th>编辑</th>
                    <th>删除</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>id</th>
                    <th>标题</th>
                    <th>作者</th>
                    <th>时间</th>
                    <th>编辑</th>
                    <th>删除</th>
                </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($newsArray as $q){ ?>
    <tr>
        <td><?php echo $q['news_id'] ?></td>
        <td><?php echo $q['title']?></td>
        <td><?php echo $q['author']?></td>
        <td><?php echo $q['date']?></td>
        <td><a href="newsModify.php?id=<?php echo $q['news_id'] ?>">编辑</a></td>
        <td><a href="newsDelete.php?id=<?php echo $q['news_id'] ?>">删除</a></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
                    <?php if(($page-1)>0){?>
                    <li ><a href="newsManager.php?page_id=<?php echo $page-1; ?>">上一页</a></li>
                    <?php }?>
                        <?php for($i=1;$i<=$pageNum;$i++){?>
                        <li class="page"><a href="newsManager.php?page_id=<?php echo $i ?>"><?php if($page==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                        <?php } ?>
                        <?php if(($page+1)<$pageNum){?>
                    <li ><a href="newsManager.php?page_id=<?php echo $page+1; ?>">下一页</a></li>
                    <?php } ?>                
                </ul>
            </div>
        </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>