<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$data['tutor_id']=(int)$_SESSION['tutor_id'];
$data['username']=$db->quote(@$_POST['user']);
$data['code']=$db->quote(@$_POST['password']);
$data['name']=$db->quote(@$_POST['name']);
$data['sex']=(int)@$_POST['sex'];
$data['nation']=$db->quote(@$_POST['nation']);
$data['tel']=$db->quote(@$_POST['tel']);
$data['email']=$db->quote(@$_POST['email']);
$data['last_school']=$db->quote(@$_POST['last_school']);
$data['degree']=$db->quote(@$_POST['degree']);
$data['occupation']=$db->quote(@$_POST['occupation']);
$data['work_time']=$db->quote(@$_POST['work_time']);
$data['promoted_time']=$db->quote(@$_POST['promoted_time']);
$data['politics']=$db->quote(@$_POST['politics']);
$data['content']=$db->quote(@$_POST['content']);
$sql="UPDATE `tutor`
SET
`username` = {$data['username']},
`name` = {$data['name']},
`code` = {$data['code']},
`sex` = {$data['sex']},
`nation` = {$data['nation']},
`occupation` = {$data['occupation']},
`promoted_time` = {$data['promoted_time']},
`work_time` = {$data['work_time']},
`politics` = {$data['politics']},
`tel` = {$data['tel']},
`email` = {$data['email']},
`last_school` = {$data['last_school']},
`degree` = {$data['degree']},
`content` = {$data['content']}
WHERE `tutor_id`={$data['tutor_id']};";
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>  
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="tutorModify.php">导师信息</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
<?php 
if($db->sqlExec($sql)>0){
    echo "修改成功<a href='tutorModify.php'>返回</a>";
}else{
    echo "修改失败<a href='tutorModify.php'>返回</a>";
}
?>
       </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>