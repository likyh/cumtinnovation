<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$project_id=(int)$_GET['id'];
$result=$db->getOne("SELECT count(*) as total 
    FROM `student` A,`stu_project` B
    where A.`student_id`=B.`student_id` and B.`project_id`={$project_id}");
if($result['total']%10==0){
    $pageNum=$result['total']/10;
}else{
    $pageNum=($result['total']/10)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*10;
$sql="SELECT `stu_project_id`, `name`, `num`, `dept`, `major`, `class`, `award`, `talent`, `tel`, `email`
FROM `student` A,`stu_project` B
where A.`student_id`=B.`student_id` and B.`project_id`={$project_id} limit $start,10;";
$projectArray=$db->getAll($sql);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="projectManager.php">项目管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
            <table id="dataTable" >
                <thead>
                    <tr>
                        <th>名字</th>
                        <th>学号</th>
                        <th>电话</th>
                        <th>邮箱</th>
                        <th>学院</th>
                        <th>专业</th>
                        <th>班级</th>
                        <th>获奖情况</th>
                        <th>个人才能</th>
                        <th>项目贡献</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>名字</th>
                        <th>学号</th>
                        <th>电话</th>
                        <th>邮箱</th>
                        <th>学院</th>
                        <th>专业</th>
                        <th>班级</th>
                        <th>获奖情况</th>
                        <th>个人才能</th>
                        <th>项目贡献</th>
                    </tr>
                </tfoot>
                <tbody>
    <?php
    foreach($projectArray as $q){ ?>
    <tr>
        <td><?php echo $q['name'] ?></td>
        <td><?php echo $q['num']?></td>
        <td><?php echo $q['tel']?></td>
        <td><?php echo $q['email']?></td>
        <td><?php echo $q['dept']?></td>
        <td><?php echo $q['major']?></td>
        <td><?php echo $q['class']?></td>
        <td><?php echo $q['award']?></td>
        <td><?php echo $q['talent']?></td>
        <td><a href="projectStudentModify.php?id=<?php echo $q['stu_project_id'] ?>">填写该学生贡献</a></td>
    </tr>
    <?php } ?>
                </tbody>
            </table>
            <div id="dataPage">
                <ul>
                    <li>共<span><?php echo (int)$pageNum; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
                    <li ><a href="projectStudent.php?id=<?php echo $project_id; ?>&page_id=<?php if(($page-1)<=0) echo $page; else echo $page-1; ?>">上一页</a></li>
                        <?php for($i=1;$i<=$pageNum;$i++){?>
                        <li class="page"><a href="projectStudent.php?id=<?php echo $project_id; ?>&page_id=<?php echo $i ?>"><?php echo $i ?></a></li>
                        <?php } ?>
                    <li ><a href="projectStudent.php?id=<?php echo $project_id; ?>&page_id=<?php if(($page+1)>=$pageNum) echo $page; else echo $page+1; ?>">下一页</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>