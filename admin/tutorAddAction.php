<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
if(empty($_POST['user'])||empty($_POST['name'])||empty($_POST['password'])){
    echo "修改失败<a href='dashboard.php'>返回</a>";
}else {
    $user=$db->quote($_POST['user']);
    $name=$db->quote($_POST['name']);
    $password=$db->quote($_POST['password']);
    $sql="INSERT INTO `tutor`
    (`username`,`name`,`code`)VALUES
    ({$user},{$name},{$password});
    ";
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>  
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="tutorAdd.php">添加导师</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
        
<?php
    if($db->sqlExec($sql)>0){
        echo "导师[{$name}]添加成功<a href='dashboard.php'>返回</a>";
    }else{
        echo "修改失败<a href='dashboard.php'>返回</a>";
    }
}
?>
       </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>