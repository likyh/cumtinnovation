<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$tutor_id=(int)$_SESSION['tutor_id'];
$project_id=$db->quote($_POST['id']);
$title=$db->quote($_POST['title']);
$start_date=$db->quote($_POST['start_date']);
$end_date=$db->quote($_POST['end_date']);
$expense=$db->quote($_POST['expense']);
$content=$db->quote($_POST['content']);
$sql="UPDATE `project` SET
`title` = {$title},
`content` = {$content},
`start_date` = {$start_date},
`end_date` = {$end_date},
`expense` = {$expense}
WHERE `tutor_id`={$tutor_id} and `project_id`={$project_id};";
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>  
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="newsManager.php">文章管理</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">

<?php
if($db->sqlExec($sql)>0){
    echo "修改成功<a href='projectManager.php'>返回</a>";
}else{
    echo "修改失败<a href='projectManager.php'>返回</a>";
}
?>
       </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>