<?php
include 'check.php';
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        $(document).ready(function () {
            UE.getEditor('contentInput');
        });
    </script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="tutorAdd.php">添加导师</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
        <form action="tutorAddAction.php" method="post">
         <fieldset>
            <legend>添加一个导师</legend>
            <label for="userInput">用户名</label>
            <input type="text" name="user" id="userInput" placeholder="请输入用户名">
            <label for =nameInput">导师姓名</label>
            <input type="text" name="name" id="nameInput" placeholder="请输入作者名">
            <label for="passwordInput">密码：</label>
            <input type="password" name="password" id="passwordInput" placeholder="输入密码">
        </fieldset>
    <input type="submit">
</form>
        </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>