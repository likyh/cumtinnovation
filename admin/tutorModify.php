<?php
include 'check.php';
include_once "../lib/DB.class.php";
$db=DealDB::init();
$id=(int)$_SESSION['tutor_id'];
$sql="SELECT * FROM tutor where `tutor_id`={$id};";
$tutor=$db->getOne($sql);
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>LikyhCMS</title>
    <link href="style/common.css" rel="stylesheet"/>
    <link href="style/table.css" rel="stylesheet"/>
    <script src="script/jquery-1.10.2.min.js" type="text/javascript"></script>

    <!-- BEGIN: load ueditor -->
    <script src="ueditor/ueditor.config.js"></script>
    <script src="ueditor/ueditor.all.min.js"></script>
    <script src="ueditor/lang/zh-cn/zh-cn.js"></script>
    <script>
        $(document).ready(function () {
            UE.getEditor('contentInput');
        });
    </script>
</head>
<body>
<div id="container">
<?php include "part/header.php"; ?>  
<?php include "part/nav.php"; ?>
    <div id="content">
        <div class="contentTitle"><h2>后台首页</h2><span>当前位置：<a href="dashboard.php">后台首页</a>&gt;<a href="tutorModify.php">导师信息</a></span></div>
            <div id="contentControl">
            </div>
        <div id="data">
        <form action="tutorModifyAction.php" method="post">
    <fieldset>
        <legend>登陆信息</legend>
        <label for ="userInput">用户名</label>
        <input type="text" name="user" id="userInput" value="<?php echo $tutor['username'] ?>" placeholder="请输入用户名">
        <label for ="passwordInput">密码</label>
        <input type="password" name="password" id="passwordInput" value="<?php echo $tutor['code'] ?>" placeholder="请输入密码">
    </fieldset>
    <fieldset>
        <legend>基本信息</legend>
        <label for ="nameInput">名称</label>
        <input type="text" name="name" id="nameInput" value="<?php echo $tutor['name'] ?>" placeholder="请输入名称">
        <label for ="sexInput">性别</label>
        <select name="sex" id="sexInput">
            <option value="1" <?php if($tutor['sex']) echo "selected" ?>>男</option>
            <option value="0" <?php if(!$tutor['sex']) echo "selected" ?>>女</option>
        </select>
        <label for ="nationInput">名族</label>
        <input type="text" name="nation" id="nationInput" value="<?php echo $tutor['nation'] ?>" placeholder="请输入名族">
        <label for ="telInput">电话</label>
        <input type="tel" name="tel" id="telInput" value="<?php echo $tutor['tel'] ?>" placeholder="请输入电话">
        <label for ="emailInput">邮箱</label>
        <input type="email" name="email" id="emailInput" value="<?php echo $tutor['email'] ?>" placeholder="请输入邮箱">
        <label for ="last_schoolInput">毕业学校</label>
        <input type="text" name="last_school" id="last_schoolInput" value="<?php echo $tutor['last_school'] ?>" placeholder="请输入毕业学校">
        <label for ="degreeInput">学位</label>
        <input type="text" name="degree" id="degreeInput" value="<?php echo $tutor['degree'] ?>" placeholder="请输入学位">
    </fieldset>
    <fieldset>
        <legend>工作信息</legend>
        <label for ="occupationInput">职位</label>
        <input type="text" name="occupation" id="occupationInput" value="<?php echo $tutor['occupation'] ?>" placeholder="请输入职位">
        <label for ="work_timeInput">开始工作时间</label>
        <input type="text" name="work_time" id="work_timeInput" value="<?php echo $tutor['work_time'] ?>" placeholder="请输入开始工作时间">
        <label for ="promoted_timeInput">晋升时间</label>
        <input type="text" name="promoted_time" id="promoted_timeInput" value="<?php echo $tutor['promoted_time'] ?>" placeholder="请输入晋升时间">
        <label for ="politicsInput">政治面貌</label>
        <input type="text" name="politics" id="politicsInput" value="<?php echo $tutor['politics'] ?>" placeholder="请输入政治面貌">
    </fieldset>
    <fieldset>
        <legend>更多信息</legend>
        <label for ="contentInput">内容</label>
        <textarea name="content" id="contentInput" placeholder="请输入内容"><?php echo $tutor['content'] ?></textarea>
    </fieldset>
    <input type="submit">
</form>
        </div>
    </div>
    <div id="siteMap">
        <ul>
            <li><a href="#">关于我们</a></li>
            <li><a href="#">联系我们</a></li>
            <li><a href="#">意见反馈</a></li>
            <li><a href="#">站长统计</a></li>
        </ul>
    </div>
    <div id="copyright">
        <p>什么依然的加一大堆，希望能够联系我们！</p>
    </div>
</div>
</body>
</html>