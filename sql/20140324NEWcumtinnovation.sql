-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 03 月 24 日 05:16
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `cumtinnovation`
--

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `news`
--

INSERT INTO `news` (`news_id`, `title`, `author`, `content`, `date`) VALUES
(1, '这是第一个新闻', '这是第一个新闻', '1', '2014-03-11'),
(2, '这是第2个新闻', '这是第2个新闻', '2', '2014-03-18'),
(3, '这是第3个新闻', '这是第3个新闻', '第一个新闻内容', '2014-03-18');

-- --------------------------------------------------------

--
-- 表的结构 `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(9) NOT NULL AUTO_INCREMENT,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT '项目名称',
  `content` text NOT NULL COMMENT '项目内容',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `expense` int(9) DEFAULT NULL COMMENT '经费',
  `stu_expense` int(9) DEFAULT NULL COMMENT '研究生经费',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `project`
--

INSERT INTO `project` (`project_id`, `tutor_id`, `title`, `content`, `start_date`, `end_date`, `expense`, `stu_expense`) VALUES
(1, 1, '这是第一个项目', '这是内容-第一个项目', '2012-12-12', '2013-12-10', 10, 8),
(2, 2, '这是第二个项目', '这是内容-第二个项目', '2013-12-12', '2014-08-28', 15, 10),
(3, 1, '这是第三个项目', '这是内容-第三个项目', '2013-12-12', '2014-08-28', 15, 10);

-- --------------------------------------------------------

--
-- 表的结构 `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `num` varchar(40) NOT NULL COMMENT '学号',
  `tutor_id` int(11) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL COMMENT '学院',
  `major` varchar(255) DEFAULT NULL COMMENT '专业',
  `class` varchar(255) DEFAULT NULL,
  `award` text COMMENT '获奖信息',
  `talent` text COMMENT '自我说明',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `student`
--

INSERT INTO `student` (`student_id`, `name`, `num`, `tutor_id`, `dept`, `major`, `class`, `award`, `talent`) VALUES
(2, '邓松高筠', '08123471', NULL, '', NULL, '', '', ''),
(3, '程义情', '08123470', NULL, '', NULL, '', '', ''),
(4, '何先婷', '08123472', NULL, '', NULL, '', '', ''),
(5, '袁笛', '08123345', NULL, '', NULL, '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `stu_project`
--

CREATE TABLE IF NOT EXISTS `stu_project` (
  `stu_project_id` int(9) NOT NULL AUTO_INCREMENT,
  `project_id` int(9) NOT NULL,
  `student_id` int(9) NOT NULL,
  `state` varchar(45) DEFAULT NULL COMMENT '项目状况，开始了 结束了 还是怎么了 自己填写吧',
  `stu_num` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`stu_project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='学生目表，有学生学号项目id' AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `stu_project`
--

INSERT INTO `stu_project` (`stu_project_id`, `project_id`, `student_id`, `state`, `stu_num`) VALUES
(7, 3, 0, NULL, '08123471'),
(8, 3, 0, NULL, '08123470'),
(9, 1, 0, NULL, '08123472'),
(10, 1, 0, NULL, '08123345');

-- --------------------------------------------------------

--
-- 表的结构 `tutor`
--

CREATE TABLE IF NOT EXISTS `tutor` (
  `tutor_id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL COMMENT '导师自定义一个用户名',
  `name` varchar(30) NOT NULL,
  `code` varchar(255) NOT NULL COMMENT '密码',
  `sex` tinyint(1) NOT NULL COMMENT '性别',
  `nation` varchar(40) DEFAULT NULL COMMENT '名族',
  `occupation` varchar(40) DEFAULT NULL COMMENT '职位',
  `promoted_time` date DEFAULT NULL COMMENT '晋升日期',
  `work_time` date DEFAULT NULL COMMENT '开始工作日期',
  `politics` varchar(40) DEFAULT NULL COMMENT '政治面貌',
  `tel` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `last_school` varchar(50) DEFAULT NULL COMMENT '毕业学校',
  `degree` varchar(30) DEFAULT NULL COMMENT '最后学位',
  PRIMARY KEY (`tutor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `tutor`
--

INSERT INTO `tutor` (`tutor_id`, `username`, `name`, `code`, `sex`, `nation`, `occupation`, `promoted_time`, `work_time`, `politics`, `tel`, `email`, `last_school`, `degree`) VALUES
(1, 'swy', '孙文远', '123456', 1, '汉族', '辅导员', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'tsl', '田顺利', '123456', 1, '汉族', '副辅导员', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `t_award`
--

CREATE TABLE IF NOT EXISTS `t_award` (
  `id` int(20) NOT NULL,
  `tutor_id` varchar(45) DEFAULT NULL,
  `title` varchar(255) NOT NULL COMMENT '获奖项目名称\n',
  `organization` varchar(255) NOT NULL COMMENT '评奖机构',
  `rank` varchar(255) NOT NULL COMMENT '获奖等级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师获奖情况';

-- --------------------------------------------------------

--
-- 表的结构 `t_essay`
--

CREATE TABLE IF NOT EXISTS `t_essay` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `tutor_id` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `publish_date` date NOT NULL,
  `publication` varchar(255) NOT NULL COMMENT '论文出版刊物',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_recruit_major`
--

CREATE TABLE IF NOT EXISTS `t_recruit_major` (
  `id` int(9) NOT NULL,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师招生专业';

-- --------------------------------------------------------

--
-- 表的结构 `t_study_direction`
--

CREATE TABLE IF NOT EXISTS `t_study_direction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(100) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师研究方向' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
