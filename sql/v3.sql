CREATE DATABASE  IF NOT EXISTS `cumtinnovation` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cumtinnovation`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: cumtinnovation
-- ------------------------------------------------------
-- Server version	5.5.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `news_id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'1','','1','2014-03-11'),(2,'2','','2','2014-03-18'),(3,'这是第一个新闻','','第一个新闻内容','2014-03-18');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(9) NOT NULL AUTO_INCREMENT,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT '项目名称',
  `content` text NOT NULL COMMENT '项目内容',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `expense` int(9) DEFAULT NULL COMMENT '经费',
  `stu_expense` int(9) DEFAULT NULL COMMENT '研究生经费',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stu_project`
--

DROP TABLE IF EXISTS `stu_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stu_project` (
  `stu_project_id` int(9) NOT NULL AUTO_INCREMENT,
  `project_id` int(9) NOT NULL,
  `student_id` int(9) NOT NULL,
  `state` varchar(45) DEFAULT NULL COMMENT '项目状况，开始了 结束了 还是怎么了 自己填写吧',
  PRIMARY KEY (`stu_project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生目表，有学生id 项目id';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stu_project`
--

LOCK TABLES `stu_project` WRITE;
/*!40000 ALTER TABLE `stu_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `stu_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `num` varchar(40) NOT NULL COMMENT '学号',
  `tutor_id` int(11) NOT NULL,
  `dept` varchar(255) NOT NULL COMMENT '学院',
  `major` varchar(255) NOT NULL COMMENT '专业',
  `class` varchar(255) DEFAULT NULL,
  `award` text COMMENT '获奖信息',
  `talent` text COMMENT '自我说明',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_award`
--

DROP TABLE IF EXISTS `t_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_award` (
  `id` int(20) NOT NULL,
  `tutor_id` varchar(45) DEFAULT NULL,
  `title` varchar(255) NOT NULL COMMENT '获奖项目名称\n',
  `organization` varchar(255) NOT NULL COMMENT '评奖机构',
  `rank` varchar(255) NOT NULL COMMENT '获奖等级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师获奖情况';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_award`
--

LOCK TABLES `t_award` WRITE;
/*!40000 ALTER TABLE `t_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_essay`
--

DROP TABLE IF EXISTS `t_essay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_essay` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `tutor_id` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `publish_date` date NOT NULL,
  `publication` varchar(255) NOT NULL COMMENT '论文出版刊物',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_essay`
--

LOCK TABLES `t_essay` WRITE;
/*!40000 ALTER TABLE `t_essay` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_essay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_recruit_major`
--

DROP TABLE IF EXISTS `t_recruit_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_recruit_major` (
  `id` int(9) NOT NULL,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师招生专业';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_recruit_major`
--

LOCK TABLES `t_recruit_major` WRITE;
/*!40000 ALTER TABLE `t_recruit_major` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_recruit_major` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_study_direction`
--

DROP TABLE IF EXISTS `t_study_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_study_direction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `tutor_id` int(9) NOT NULL,
  `title` varchar(100) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导师研究方向';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_study_direction`
--

LOCK TABLES `t_study_direction` WRITE;
/*!40000 ALTER TABLE `t_study_direction` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_study_direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `tutor_id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL COMMENT '导师自定义一个用户名',
  `name` varchar(30) NOT NULL,
  `code` varchar(255) NOT NULL COMMENT '密码',
  `sex` tinyint(1) NOT NULL COMMENT '性别',
  `nation` varchar(40) DEFAULT NULL COMMENT '名族',
  `occuption` varchar(40) DEFAULT NULL COMMENT '职位',
  `promoted_time` date DEFAULT NULL COMMENT '晋升日期',
  `work_time` date DEFAULT NULL COMMENT '开始工作日期',
  `politics` varchar(40) DEFAULT NULL COMMENT '政治面貌',
  `tel` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `last_school` varchar(50) DEFAULT NULL COMMENT '毕业学校',
  `degree` varchar(30) DEFAULT NULL COMMENT '最后学位',
  PRIMARY KEY (`tutor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-21 18:32:39
