-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 03 月 19 日 06:17
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `cumtinnovation`
--
CREATE DATABASE `cumtinnovation` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cumtinnovation`;

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(10) NOT NULL,
  `teacher_name` varchar(20) NOT NULL,
  `teacher_number` varchar(10) NOT NULL,
  `dept` varchar(40) NOT NULL,
  `profession` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_article`
--

CREATE TABLE IF NOT EXISTS `t_article` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` int(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time` date NOT NULL,
  `post_name` varchar(200) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `teacher_number` varchar(20) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_course`
--

CREATE TABLE IF NOT EXISTS `t_course` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `teacher_number` varchar(20) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `last_time` varchar(50) NOT NULL,
  `target` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_lead_profession`
--

CREATE TABLE IF NOT EXISTS `t_lead_profession` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `teacher_number` varchar(20) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_prize`
--

CREATE TABLE IF NOT EXISTS `t_prize` (
  `id` int(20) NOT NULL,
  `number` varchar(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `place` varchar(100) NOT NULL,
  `grade` varchar(70) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `teacher_name` varchar(30) NOT NULL,
  `teacher_number` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `t_progect`
--

CREATE TABLE IF NOT EXISTS `t_progect` (
  `progect_id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `teacher_number` varchar(30) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`progect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_study_direction`
--

CREATE TABLE IF NOT EXISTS `t_study_direction` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enable` tinyint(4) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  `teacher_number` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `t_study_project`
--

CREATE TABLE IF NOT EXISTS `t_study_project` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `start_time` date NOT NULL,
  `over_time` date NOT NULL,
  `expense` varchar(20) NOT NULL,
  `study_expense` varchar(20) NOT NULL,
  `teacher_number` varchar(30) NOT NULL,
  `teacher_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `teacher_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `number` varchar(20) NOT NULL,
  `dept` varchar(30) NOT NULL,
  `enable` int(11) NOT NULL DEFAULT '1',
  `sex` varchar(5) NOT NULL,
  `nation` varchar(10) NOT NULL,
  `tem_occuption` varchar(40) NOT NULL,
  `promoted_time` date NOT NULL,
  `work_time` date NOT NULL,
  `politics` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `last_study_place` varchar(50) NOT NULL,
  `last_degree` varchar(30) NOT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
