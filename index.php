<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$sql="select * from `news` where 1 order by `date` limit 5";
$result=$db->getAll($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/index.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
            <div class="news">
                <div id="imageNews" class="block">
                    <img src="images/7.png" alt="123"/>
                    <div id="imageTitle">
                        <p>基本信息、基本特长、竞赛信息（已获奖和正在参与）</p>
                    </div>
                </div>
                <div id="textNews" class="block">
                    <h2>通知公告</h2>
                    <?php if(!empty($result)){ ?>
                    <?php  foreach ($result as $value) {?>
                     <h3><?php echo $value['date'] ?><a href="newsInfo.php?id=<?php echo $value['news_id']; ?>"><?php echo $value['title'] ?></a></h3>
                    <?php } }else echo "暂时没有通知";?>
                </div>
            </div>
            <div class="information">
                <div class="match left block"><a href="conList.php" title="竞赛报名"><img src="images/1.png" alt="竞赛报名"/></a></div>
                <div class="student block"><a href="" title="学生信息"><img src="images/2.png" alt="学生信息"/></a></div>
                <div class="project left block"><a href="proList.php" title="项目信息"><img src="images/3.png" alt="项目信息"/></a></div>
                <div class="teacher block"><a href="tuList.php" title="导师信息"><img src="images/4.png" alt="导师信息"/></a></div>
            </div>
            <div class="display">
                <div class="achievement left block"><a href="" title="成果展示"><img src="images/5.png" alt="成果展示"/></a></div>
                <div class="displayBlock block"><a href="" title="1"><img src="images/6.png" alt="1"/></a></div>
                <div class="displayBlock long block"><a href="" title="2"><img src="images/7.png" alt="2"/></a></div>
                <div class="displayBlock left block"><a href="" title="3"><img src="images/6.png" alt="3"/></a></div>
                <div class="displayBlock block"><a href="" title="4"><img src="images/6.png" alt="4"/></a></div>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>