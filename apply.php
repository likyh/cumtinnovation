<?php
include_once "lib/DB.class.php";
 $db=DealDB::init();
if(isset($_POST['project_id'])&&!empty($_POST['project_id']) && isset($_POST['num'])&&!empty($_POST['num'])){
    $project_id=(int)$_POST['project_id'];
    $num=$db->quote($_POST['num']);
    $existSql="SELECT * FROM student where `num`={$num};";
    if(!$db->getExist($existSql)){
        $insertSql="INSERT INTO `student` (`num`)VALUES({$num});";
        $db->sqlExec($insertSql);
    }
    $selectSql="SELECT `student_id`, `name`, `dept`, `major`, `class`, `award`, `talent`, `tel`, `email`
        FROM `student` where `num`={$num}";
    $student=$db->getOne($selectSql);
}else{
    echo "提交失败<a href='index.php'>返回</a>";
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="text">
                    <h1>学生报名项目</h1>
                    <form action="applyResult.php" method="post">
                        <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
                        <input type="hidden" name="student_id" value="<?php echo $student['student_id'] ?>">
                        <fieldset>
                            <legend>基本信息</legend>
                            <label for ="nameInput">姓名</label>
                            <input type="text" name="name" id="nameInput" value="<?php echo $student['name'] ?>" placeholder="请输入姓名">
                            <label for ="telInput">电话</label>
                            <input type="tel" name="tel" id="telInput" value="<?php echo $student['tel'] ?>" placeholder="请输入电话">
                            <label for ="emailInput">邮箱</label>
                            <input type="email" name="email" id="emailInput" value="<?php echo $student['email'] ?>" placeholder="请输入邮箱">
                            <label for ="deptInput">学院</label>
                            <input type="text" name="dept" id="deptInput" value="<?php echo $student['dept'] ?>" placeholder="请输入学院">
                            <label for ="majorInput">专业</label>
                            <input type="text" name="major" id="majorInput" value="<?php echo $student['major'] ?>" placeholder="请输入专业">
                            <label for ="classInput">班级</label>
                            <input type="text" name="class" id="classInput" value="<?php echo $student['class'] ?>" placeholder="请输入班级">
                        </fieldset>
                        <fieldset>
                            <legend>更多信息</legend>
                            <label for ="awardInput">请输入获奖情况</label>
                            <textarea name="award" id="awardInput"><?php echo $student['award'] ?></textarea>
                            <label for ="talentInput">个人才能、特长、爱好等等其他信息</label>
                            <textarea name="talent" id="talentInput"><?php echo $student['talent'] ?></textarea>

                        </fieldset>
                        <input type="submit" value="提交"> 
                    </form>
                </article>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>