<?PHP
include_once "config.php";
class DealDB {
	private static $db;
	private static $dealDB;

	
	/**
	* 构造函数，获取pdo引用
	* @return pdo对象引用
	*/
// 	private 
	function __construct() {
		// 检测是否已经新建

		if(isset(self::$db)) return;
		
		global $config;
		$host = $config['mysql']["host"];
		$port = $config['mysql']["port"];
		$user = $config['mysql']["username"];
		$pass = $config['mysql']["password"];
		$dbname = $config['mysql']["dbname"];

		try {
			$DSN = "mysql:host={$host};port={$port};dbname={$dbname}";
			$handle = new PDO($DSN,$user,$pass);
			$handle -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			$handle-> query("set names utf8"); 
		} catch(PDOException $e) {
			echo "Connection failed:".$e->getMessage();
		}
		self::$db = $handle;
		return;
	}
	
	/**
	 * 获取数据库操作类的饮用对象
	 */
	static public function init(){
		if(!isset(self::$dealDB)){
			self::$dealDB= new DealDB();
		}
		return self::$dealDB;
	}
	
	/**
	* 函数直接执行sql语句
	* @param string $sql 要执行的SQL语句
	* @return int 返回修改的行数
	*/
	function sqlExec($sql){
		$result=self::$db->exec($sql);
		return $result;
	}
	
	/**
	* 检查sql语句是否能查询到内容
	* @param string $sql 要执行的SQL语句
	* @return bool 是否查询到内容
	*/
	function getExist($sql){
		$tmp=self::$db->query($sql);
		$ifGet = $tmp->fetch(PDO::FETCH_BOUND);
		$ifGetText=$ifGet?"True":"False";
		return $ifGet;		
	}

	
	/**
	* 函数使用sql语句获取一行数据
	* @param string $sql 要执行的SQL语句
	* @return array 一个1维数组
	*/
	function getOne($sql){
		$tmp=self::$db->query($sql);
		if(!empty($tmp)){
			$array= $tmp->fetch(PDO::FETCH_ASSOC);
				return $array;
		}else{
			return array();
		}
	}
	
	/**
	* 函数使用sql语句获取所有查询到的数据
	* @param string $sql 要执行的SQL语句
	* @return array 一个2维数组
	*/
	function getAll($sql){
		$tmp=self::$db->query($sql);
		if(!empty($tmp)){
			$array= $tmp->fetchall(PDO::FETCH_ASSOC);
			//TODO 这一句好像效率有一点差
			return $array;
		}else{
			return array();
		}
	}

	/**
	 * 对一个字符串加引号并过滤
	 * @param string $s
	 */
	function quote($s){
		return self::$db->quote($s);
	}

	/**
	* 插入数据
	* @param string $tableName 要插入的表名
	* @param string $data 要插入的数组，键名为数据库字段名，值为要插入的值
	* @return int 插入了多少内容
	*/
	function insert($tableName,$data){
		$db=self::$db;

		$line="";	//插入的列
		$value="";	//插入的值
		if(!is_array($data) || count($data)<=0){
			return false; //没有要插入的数据
		}else{		
			foreach($data as $key=>$result){			
				$line_arr[]=$key;
				$value_arr[]=$db->quote($result);
			}
			$line=implode(",",$line_arr);
			$value=implode(",",$value_arr);
			$sql="INSERT INTO {$tableName}($line) values($value);";
			return $this->sqlExec($sql);
		}
	}

	/**
	* 更新数据
	* @param string $tableName 表名
	* @param int $id id号，如果id不存在则修改失败
	* @param string $data 要更新的数据，键名为数据库字段名，值为要插入的值
	* @return int 修改了多少内容
	*/
	function update($tableName,$id,$data){
		$db=self::$db;
		
		//查询id号，如果id不存在则修改失败
		$up_arr=array();	//更新的语句
		if(!is_array($data) || count($data)<=0){
			return 0; //没有要插入的数据
		}else{
			if(empty($data['modify_time'])){
				$data['modify_time']=date('Y-m-d H:i:s');
			}
			foreach($data as $key=>$value){
				$up_arr[]="`{$key}`=".$db->quote($value);
			}
			$up_str=implode(",",$up_arr);
			$sql="UPDATE {$tableName} set {$up_str} where `id`={$id} and `enable`='1'";
// 			var_dump($sql);
			return $this->sqlExec($sql);
		}
	}

	/**
	* 添加或者更新数据，根据id存在与否新建或者修改一条数据
	* @param string $tableName 表名
	* @param int $id id号，如果id不存在则新建一行数据
	* @param string $data 要更新的数据，键名为数据库字段名，值为要插入的值
	* @return int 修改了多少内容
	*/
	function modify($tableName,$id,$data){
		// 先修改，若没有修改成功则插入一行
		$updateModifyNum=$this->update($tableName,$id,$data);
		// var_dump($updateModifyNum);
		if($updateModifyNum){
			return $updateModifyNum;
		}else{
			return $this->insert($tableName,$data);
		}
	}

	/**
	* 删除数据
	* @param string $tableName 表名
	* @param int $id
	* @return int 删除了多少内容
	*/
	function delete($tableName,$id){
		$sql="DELETE from {$tableName} where id={$id}";
		return $this->sqlExec($sql);
	}
}