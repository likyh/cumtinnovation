<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$project_id=isset($_GET['project_id'])&&!empty($_GET['project_id']) ? (int)$_GET['project_id'] : '1';
$student_id=isset($_GET['student_id'])&&!empty($_GET['student_id']) ? (int)$_GET['student_id'] : '1';
$sql="SELECT `name`,`num`,`state`,`content`
    FROM stu_project,student
    WHERE stu_project.student_id=student.student_id 
    and stu_project.project_id=$project_id
    and stu_project.student_id=$student_id";
$student=$db->getOne($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="text">
                    <h1><?php echo $student['name'] ?> - <?php echo $student['num'] ?> [<?php echo $student['state'] ?>]</h1>
                    <?php echo $student['content'] ?>
                </article>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>