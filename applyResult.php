<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$project_id=isset($_POST['project_id'])&&!empty($_POST['project_id']) ? (int)$_POST['project_id'] : 1;
$student_id=(int)$_POST['student_id'];
$name=empty($_POST['name'])?'':htmlentities($_POST['name'],ENT_NOQUOTES,"utf-8");
$tel=empty($_POST['tel'])?'':htmlentities($_POST['tel'],ENT_NOQUOTES,"utf-8");
$email=empty($_POST['email'])?'':htmlentities($_POST['email'],ENT_NOQUOTES,"utf-8");
$dept=empty($_POST['dept'])?'':htmlentities($_POST['dept'],ENT_NOQUOTES,"utf-8");
$major=empty($_POST['major'])?'':htmlentities($_POST['major'],ENT_NOQUOTES,"utf-8");
$class=empty($_POST['class'])?'':htmlentities($_POST['class'],ENT_NOQUOTES,"utf-8");

$award=empty($_POST['award'])?'':htmlentities($_POST['award'],ENT_NOQUOTES,"utf-8");
$talent=empty($_POST['talent'])?'':htmlentities($_POST['talent'],ENT_NOQUOTES,"utf-8");

$checkIfApply="select * from `stu_project` where `student_id`=$student_id and `project_id`='$project_id'";
// var_dump($checkIfApply);
$r_checkIfApply=$db->getOne($checkIfApply);  //看该学生是否已经报过这个项目
if($r_checkIfApply>0){
    $message="已经报名过了";
}else{

    $sql="UPDATE `student` SET `name` = '{$name}',`dept` = '{$dept}',`class` = '{$class}',`major` = '{$major}',`award` = '{$award}',
  `talent` = '{$talent}',`tel` = '{$tel}',`email` = '{$email}' WHERE `student_id` ={$student_id};";
//var_dump($sql);
    if($db->sqlExec($sql)>0){
        $message="修改成功<a href='projectManager.php'>返回</a>";
    }else{
        $message="修改失败<a href='projectManager.php'>返回</a>";
    }

    $stuProjectArr=array(
            'project_id' =>$project_id ,
            'student_id' =>$student_id
        );
//    var_dump($stuProjectArr);
    $insertStuProject=$db->insert('stu_project',$stuProjectArr);
    if($insertStuProject){
        $message="报名成功，请主动打电话联系导师<a href='index.php'>返回</a>";
    }else {
        $message="报名失败<a href='index.php'>返回</a>";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="text">
                    <h1>报名结果</h1><br/>
                    <p><?php echo $message ?></p>
                </article>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>