<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$result=$db->getOne("select count(*) as total from `news` where 1");
if($result['total']%10==0){
    $pageNum=$result['total']/10;
}else{
    $pageNum=($result['total']/10)+1;
}
$page=isset($_GET['page_id'])? (int)$_GET['page_id'] :'1';
$start=((int)$page==1)? '0':($page-1)*10;
$sql="select `news_id`,`title`,`date` from `news` where 1 order by `date` limit $start,10";
$result=$db->getAll($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/list.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <h2>通知公告</h2>
                <ul class="substance">
                    <?php if(!empty($result)){ foreach ($result as $v) { ?>
                        <li><a href="newsInfo.php?id=<?php echo $v['news_id']; ?>"><?php echo $v['title'] ?></a><?php echo $v['date'] ?></li> 
                    <?php }}else echo "暂时没有通知公告"; ?>
                </ul>
                <div id="page">
                    <ul><?php if(!empty($result)){?>
                        <!-- <li><a href="#">第一页</a></li> -->      
                        <li ><a href="newsList.php?page_id=<?php if(($page-1)<=0) echo $page; else echo $page-1; ?>">上一页</a></li>
                        <?php for($i=1;$i<=$pageNum;$i++){?>
                        <li><a href="newsList.php?page_id=<?php echo $i ?>"><?php echo $i ?></a></li>
                        <?php } ?>
                        <li ><a href="newsList.php?page_id=<?php if(($page+1)>=$pageNum) echo $page; else echo $page+1; ?>">下一页</a></li>
                        <?php }  ?>
                        <!-- <li><a href="#">最后一页</a></li> -->
                    </ul>
                </div>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>