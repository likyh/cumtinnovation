<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$tutor_id=isset($_GET['id'])&&!empty($_GET['id']) ? (int)$_GET['id'] : '1';
$sql="select * from `tutor` where `tutor_id`=$tutor_id";
$result=$db->getOne($sql);
$sql="select ";//把导师的获奖情况，什么论文之类的也要查出来
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="table">
                    <h1>导师信息 - <?php echo $result['name'] ?></h1>
                    <div id="baseInformation">
                        <h2>基本信息</h2>
                        <table>
                            <tr>
                                <th class="tg-031e">姓名</th>
                                <td class="tg-031e"><?php echo $result['name'] ?></td>
                                <th class="tg-031e">性别</th>
                                <td class="tg-031e"><?php echo $result['sex']?"男":"女"; ?></td>
                                <th class="tg-031e">名族</th>
                                <td class="tg-031e"><?php echo $result['nation'] ?></td>
                                <th class="tg-031e">政治面貌</th>
                                <td class="tg-031e"><?php echo $result['politics'] ?></td>
                            </tr>
                            <tr>
                                <th class="tg-031e">现任专业技术职务</th>
                                <td class="tg-031e"><?php echo $result['occupation'] ?></td>
                                <th class="tg-031e">晋升时间</th>
                                <td class="tg-031e"><?php echo $result['promoted_time'] ?></td>
                                <th class="tg-031e">参加工作时间</th>
                                <td class="tg-031e"><?php echo $result['work_time'] ?></td>
                                <th class="tg-031e">学历</th>
                                <td class="tg-031e"><?php echo $result['last_school'] ?></td>
                            </tr>
                            <tr>
                                <th class="tg-031e">联系电话</th>
                                <td class="tg-031e" colspan="3"><?php echo $result['tel'] ?></td>
                                <th class="tg-031e">Email</th>
                                <td class="tg-031e" colspan="3"><?php echo $result['email'] ?></td>
                            </tr>
                        </table>
                    </div>
                    <div id="moreInformation">
                        <h2>更多信息</h2>
                        <?php echo $result['content'] ?>
                    </div>
                </article>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>