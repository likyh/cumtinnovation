<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$news_id=isset($_GET['id'])&&!empty($_GET['id']) ? (int)$_GET['id'] : '1';
$sql="select `title`,`author`,`content`,`date` from `news` where `news_id`=$news_id";
$result=$db->getOne($sql);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="text">
                    <h1>项目内容</h1>
                    <h3><?php echo $result['title'] ?></h3>
                    <p><?php echo $result['author'] ?></p>
                     <p>时间: <?php echo $result['date'] ?></p>
                    <p><?php echo $result['content'] ?></p>                   
                </article>
            </div>
        </div>

    <?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>