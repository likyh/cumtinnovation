<?php
include_once "lib/DB.class.php";
$db=DealDB::init();
$project_id=isset($_GET['id'])&&!empty($_GET['id']) ? (int)$_GET['id'] : '1';
$sql="select `project_id`,`title`,`project`.`content`,`start_date`,`end_date`,`iscontest`,`name` from `project`,`tutor` where `project_id`=$project_id";
$project=$db->getOne($sql);
$sql="SELECT stu_project.`project_id`,stu_project.`student_id`,`name`,`num`,`dept`,`major`,`class` 
    FROM stu_project,student,project
    WHERE stu_project.student_id=student.student_id 
    and stu_project.project_id=$project_id 
    and project.project_id=stu_project.project_id";
$student=$db->getAll($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>中国矿业大学创新实验中心管理系统</title>
    <link href="style/reset.css" rel="stylesheet"/>
    <link href="style/index.css" rel="stylesheet"/>
    <script src="script/reset.js"></script>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/content.js"></script>
</head>
<body>
<div id="container">
    <div class="bg">
    <?php include "part/header.php"; ?>

        <div id="main">
        <?php include "part/nav.php"; ?>
            <div id="substance">
                <article class="text">
                    <h1>项目内容</h1>
                    <h3><?php echo $project['title'] ?></h3>
                    <p>导师：<?php echo $project['name'] ?></p>

                    <p>开始时间: <?php echo $project['start_date'] ?></p>
                    <p>结束时间：<?php echo $project['end_date'] ?></p>

                    <form action="apply.php" method="post">
                        <input type="hidden" name="project_id" value="<?php echo $project['project_id'] ?>">
                        <label for ="numInput">学号</label>
                        <input type="text" name="num" id="numInput" placeholder="请输入你小子的学号">
                        <input type="submit" value="点击报名">
                    </form>
                    <br/>

                    <p><?php echo $project['content'] ?></p>
                    <h2>学生报名详情</h2>
                    <table border="1">
                        <thead>
                        <tr>
                            <th>学号</th>
                            <th>名字</th>
                            <th>学院</th>
                            <th>专业</th>
                            <th>班级</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($student){ foreach ($student as $v) { ?>
                        <tr>
                            <th><?php echo $v['num'] ?></th>
                            <th><a href="stu.php?project_id=<?php echo $v['project_id'] ?>&student_id=<?php echo $v['student_id'] ?>"><?php echo $v['name'] ?></a></th>
                            <th><?php echo $v['dept'] ?></th>
                            <th><?php echo $v['major'] ?></th>
                            <th><?php echo $v['class'] ?></th>
                        </tr>
                        <?php }} ?>                        
                        </tbody>
                    </table>
                    </article>
            </div>
        </div>

<?php include "part/footer.php"; ?>
    </div>
</div>
</body>
</html>